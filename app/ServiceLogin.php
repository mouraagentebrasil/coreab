<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceLogin extends Model
{
    //
    protected $table = 'service_login';
    protected $primaryKey='service_login_id';
    public $timestamp = false;
}
