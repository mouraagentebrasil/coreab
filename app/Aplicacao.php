<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aplicacao extends Model
{
    //
    protected $table = 'aplicacao';
    protected $primaryKey='aplicacao_id';
    public $timestamp = false;
}
