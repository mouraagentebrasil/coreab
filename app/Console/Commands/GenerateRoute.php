<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;

class GenerateRoute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geraroutes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $adminID = $this->setToAdmin();

        foreach(Route::getRoutes()->getRoutes() as $route)
        {
            $action = $route->getAction();
            $rota = $action['controller'];
            $part = explode("@",$rota);
            $part2 = explode("\\",$part[0]);
            $part3 = explode("Controller",$part2[4]);
            $method = $part[1];
            $class = $part3[0];

            $msg = $method." on ".$class;
            $route = $class."@".$method;
            $valid = $this->getRoute($rota);
            if($valid) continue;
            $routeID = $this->saveRoute($rota,$msg);
            DB::table('grupo_rota')->insert(['rota_id'=>$routeID,'grupo_id'=>$adminID,'permitido'=>1]);
        }
        echo "\nFinalizado!\n";
    }

    public function getRoute($route){

        $data = DB::table('rota')->where('route',$route)->get();
        if(empty($data)) return false;
        return true;
    }

    public function saveRoute($route,$descrition){
        $save = DB::table('rota')->insertGetId(['route'=>$route,'descricao'=>$descrition]);
        return $save;
    }

    public function setToAdmin(){
        $checkAdmin = DB::table('grupo')->where('grupo_nome','admin')->get();
        if(empty($checkAdmin)){
            $saveAdminGroup = DB::table('grupo')->insertGetId(['grupo_nome'=>'admin','grupo_criacao'=>date('Y-m-d H:i:s')]);
            return $saveAdminGroup;
        }
        return $checkAdmin[0]->grupo_id;
    }


}
